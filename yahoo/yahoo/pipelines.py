import csv
from .items import YahooItem

class YahooPipeline(object):

	def open_spider(self, spider):
		if spider.name == 'products':
			self.filename = open('yahoo_restaurants_data.csv',mode='a',encoding = 'utf-8')
			self.csv_writer = csv.writer(self.filename, quoting=csv.QUOTE_MINIMAL)
			self.csv_writer.writerow([
					'Shop Name',
					'Location',
					'Contact',
					'Access',
					'Category',
					'Rating',
					'Price_at_Dinner',
					'Price_at_Lunch',
					'Business hours',
					'Holidays',
					'Paypay flag',
					'URL'
				])
		elif spider.name == 'shops':
			self.filename = open('yahoo_shops_data.csv',mode='a',encoding = 'utf-8')
			self.csv_writer = csv.writer(self.filename, quoting=csv.QUOTE_MINIMAL)
			self.csv_writer.writerow([
					'Shop Name',
					'Location',
					'Contact',
					'Access',
					'Category',
					'Rating',
					'Price_at_Dinner',
					'Price_at_Lunch',
					'Business hours',
					'Holidays',
					'Paypay flag',
					'URL'
				])
		elif spider.name == 'games':
			self.filename = open('yahoo_games_data.csv',mode='a',encoding = 'utf-8')
			self.csv_writer = csv.writer(self.filename, quoting=csv.QUOTE_MINIMAL)
			self.csv_writer.writerow([
					'Shop Name',
					'Location',
					'Contact',
					'Access',
					'Category',
					'Rating',
					'Price_at_Dinner',
					'Price_at_Lunch',
					'Business hours',
					'Holidays',
					'Paypay flag',
					'URL'
				])
		elif spider.name == 'hotels':
			self.filename = open('yahoo_hotels_data.csv',mode='a',encoding = 'utf-8')
			self.csv_writer = csv.writer(self.filename, quoting=csv.QUOTE_MINIMAL)
			self.csv_writer.writerow([
					'Shop Name',
					'Location',
					'Contact',
					'Access',
					'Category',
					'Rating',
					'Price_at_Dinner',
					'Price_at_Lunch',
					'Business hours',
					'Holidays',
					'Paypay flag',
					'URL'
				])
		elif spider.name == 'business':
			self.filename = open('yahoo_business_data.csv',mode='a',encoding = 'utf-8')
			self.csv_writer = csv.writer(self.filename, quoting=csv.QUOTE_MINIMAL)
			self.csv_writer.writerow([
					'Shop Name',
					'Location',
					'Contact',
					'Access',
					'Category',
					'Rating',
					'Price_at_Dinner',
					'Price_at_Lunch',
					'Business hours',
					'Holidays',
					'Paypay flag',
					'URL'
				])
		elif spider.name == 'total':
			self.filename = open('yahoo_total_data.csv',mode='a',encoding = 'utf-8')
			self.csv_writer = csv.writer(self.filename, quoting=csv.QUOTE_MINIMAL)
			self.csv_writer.writerow([
					'Shop Name',
					'Location',
					'Contact',
					'Access',
					'Category',
					'Rating',
					'Price_at_Dinner',
					'Price_at_Lunch',
					'Business hours',
					'Holidays',
					'Paypay flag',
					'URL'
				])



	def close_spider(self, spider):
		if spider.name == 'products':
			self.filename.close()
		elif spider.name == 'shops':
			self.filename.close()
		elif spider.name == 'games':
			self.filename.close()
		elif spider.name == 'hotels':
			self.filename.close()
		elif spider.name == 'business':
			self.filename.close()
		elif spider.name == 'total':
			self.filename.close()

	def process_item(self, item, spider):
		if isinstance(item, YahooItem):
			self.csv_writer.writerow([
				item['shop_name'],
				item['shop_location'],
				item['shop_contact'],
				item['shop_access'],	
				item['shop_category'],
				item['rating'],
				item['price_dinner'],
				item['price_lunch'],
				item['shop_business_hours'],
				item['shop_holidays'],
				item['paypay_flag'],
				item['page_url']
			])

		return item