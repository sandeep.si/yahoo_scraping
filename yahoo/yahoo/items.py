# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class YahooItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    shop_name = scrapy.Field()
    shop_location = scrapy.Field()
    shop_contact = scrapy.Field()
    shop_access = scrapy.Field()
    shop_category = scrapy.Field()
    rating = scrapy.Field()
    price_dinner = scrapy.Field()
    price_lunch = scrapy.Field()
    shop_business_hours = scrapy.Field()
    shop_holidays = scrapy.Field()
    paypay_flag = scrapy.Field()
    page_url = scrapy.Field()

    

    

    
    
    
