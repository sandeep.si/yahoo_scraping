# -*- coding: utf-8 -*-
import scrapy
import re
import json
import codecs
from scrapy.http import HtmlResponse
from ..items import YahooItem
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError,TimeoutError, TCPTimedOutError
from scraper_api import ScraperAPIClient


class ProductsSpider(scrapy.Spider):
	name = 'hotels'
	allowed_domains = ['loco.yahoo.co.jp','api.scraperapi.com']
	
	
	main_url = 'https://loco.yahoo.co.jp'
	client = ScraperAPIClient('7cb5caf807e61f97b73f19fc4a0414b1')
	
	def start_requests(self):

		prefecture_id = '01'
		area_count = 21
		genre_id = '04'
		genre_count = 26
		
		client = ScraperAPIClient('7cb5caf807e61f97b73f19fc4a0414b1')
		# url = 'https://loco.yahoo.co.jp/search/?areacd=101040000&genrecd=0102&paypay=1'
		# yield scrapy.Request(url=url, callback=self.parse_initial, method='GET', errback=self.errback_httpbin)
		# yield scrapy.Request(client.scrapyGet(url=url), callback=self.parse_initial, method='GET', errback=self.errback_httpbin)

		for i in range(1,22):
			areacd = str(1) + prefecture_id + '{:02}'.format(i) + '0000'
			for j in range(1,27):
				genrecd = genre_id + '{:02}'.format(j)
				url = 'https://loco.yahoo.co.jp/search/?genrecd='+genrecd+'&areacd='+areacd+'&paypay=1'
				# yield scrapy.Request(url=url, callback=self.parse_initial, method='GET', errback=self.errback_httpbin)
				yield scrapy.Request(client.scrapyGet(url=url), callback=self.parse_initial, method='GET', errback=self.errback_httpbin)



	def parse_initial(self, response):

		print(response.url)

		cards = response.css('div.tracked_mods div.card__item')

		for card in cards:
			
			
			card_name = card.css('h3.card__name a::text').extract_first()
			card_access = card.css('div.card__access::text').extract_first()
			card_rating = card.css('span.cardReview__number::text').extract_first()
			card_price_dinner = card.css('div.cardPrice__text--dinner::text').extract_first()
			card_price_lunch = card.css('div.cardPrice__text--lunch::text').extract_first()
			# card_price_paypay = card.css('div.cardPrice__text--paypay::text').extract_first()
			# card_category = card.css('div.card__category::text').extract_first()
			
			card_dict = dict()
			card_dict['card_name'] = card_name
			card_dict['card_access'] = card_access
			card_dict['card_rating'] = card_rating
			card_dict['card_price_dinner'] = card_price_dinner
			card_dict['card_price_lunch'] = card_price_lunch

			print(card_dict)

			client = ScraperAPIClient('7cb5caf807e61f97b73f19fc4a0414b1')

			shop_link = card.css('h3.card__name a::attr(href)').extract_first()
			print(shop_link)
			# url = 'https://loco.yahoo.co.jp/place/g-VWOepjDF0Ak/'
			# yield scrapy.Request(client.scrapyGet(url = url), self.parse_shop)

			
			shop_url = ''		
			if shop_link is not None:
				shop_url = self.main_url + shop_link
				card_dict['shop_url'] = shop_url
				yield scrapy.Request(client.scrapyGet(url=shop_url), callback=self.parse_shop, method='GET', errback=self.errback_httpbin, meta=card_dict)
				# yield scrapy.Request(url=shop_url, callback=self.parse_shop, method='GET', errback=self.errback_httpbin, meta=card_dict)

		pagination_items = response.css('ul.pagination li.pagination__item')
		if len(pagination_items) > 0:
			for item in pagination_items:
				next_page_attr = item.css('a::attr(data-ylk)').extract_first()
				if next_page_attr is not None:
					if next_page_attr == 'slk:pn_next':
						pagination = item.css('a::attr(href)').extract_first()
						if pagination is not None:
							url = self.main_url + pagination
							# yield scrapy.Request(url=url, callback=self.parse_initial, method='GET', errback=self.errback_httpbin)
							yield scrapy.Request(client.scrapyGet(url=url), callback=self.parse_initial, method='GET', errback=self.errback_httpbin)



	def parse_shop(self,response):

		print(response.url)

		page_url = response.meta["shop_url"]

		# data_regex = re.compile(r'"address":(.*\n*.*\n*.*\n*.*\n*.*\n*.*\n*.*\n*.*)},')
		# data_item = data_regex.findall(codecs.decode(response.body, encoding='utf-8',errors='ignore'))
		# location_data = None
		# if len(data_item)>0:
		# 	location_data = json.loads(data_item[0]+'}')

		jsondata = response.css('input#tenpojson::attr(value)').extract_first()
		data = None
		if jsondata is not None:
			data = json.loads(jsondata)

		data_list = data.get('Feature',[])

		address = None

		if len(data_list) > 0:
			property_info = data_list[0].get('Property')
			if property_info is not None:
				address = property_info.get('Address')
		
		# address = data['Feature'][0]['Property']['Address']


		# postal_code = None
		# address_region = None
		# street_address = None
		# address_country = None
		# address_locality = None
		# if location_data is not None:
		# 	postal_code = location_data.get('postalCode')
		# 	address_locality = location_data.get('addressLocality')
		# 	address_region = location_data.get('addressRegion')
		# 	street_address = location_data.get('streetAddress')
		# 	address_country = location_data.get('addressCountry')
		# if address_region == None:
		# 	address_region = 'さいたま市'


		item = YahooItem()

		# location = postal_code + ' ' + address_region + ' ' + address_locality + ' ' + street_address
		location = address
		contact = ''
		category = ''
		business_hours = ''
		holidays = ''
		paypay_flag = 1

		place_table = response.css('table#placetable tr.placeData__row')
		for row in place_table:
			row_head = row.css('th.placeData__header::text').extract_first()
			if row_head == '電話番号':
				contact = row.css('td.placeData__data span::text').extract_first()
			elif row_head == '営業時間':
				business_hours = row.css('td.placeData__data::text').extract_first()
			elif row_head == 'カテゴリ':
				category = row.css('td.placeData__data::text').extract_first()
		external_table = response.css('table#external__table tr.placeData__row')
		for row in external_table:
			row_head = row.css('th.placeData__header::text').extract_first()
			if row_head == '定休日':
				holidays = row.css('td.placeData__data::text').extract_first()



		item['shop_name'] = self.filter_text(response.meta["card_name"])
		item['shop_location'] = self.filter_text(location)
		item['shop_contact'] = contact
		item['shop_access'] = self.filter_text(response.meta["card_access"])
		item['shop_category'] = self.filter_text(category)
		item['rating'] = self.filter_text(response.meta["card_rating"])
		item['price_dinner'] = self.filter_text(response.meta["card_price_dinner"])
		item['price_lunch'] = self.filter_text(response.meta["card_price_lunch"])
		item['shop_business_hours'] = self.filter_text(business_hours)
		item['shop_holidays'] = self.filter_text(holidays)
		item['paypay_flag'] = paypay_flag
		item['page_url'] = page_url

		print(item)

		yield item
	   
				

	def filter_text(self, text):
		if text is None:
			text = '-'
		text = text.replace(u'\\u3000', u' ')
		text = text.replace(u'\\xa0', u' ')
		text = text.replace(u'\\n', u' ')
		text = ' '.join(text.split())
		if text == '':
			text = '-'	
		return text

	
	def errback_httpbin(self, failure):
		# log all failures
		self.logger.error(repr(failure))
		# in case you want to do something special for some errors,
		# you may need the failure's type:
		if failure.check(HttpError):
			# these exceptions come from HttpError spider middleware
			# you can get the non-200 response
			response = failure.value.response
			self.logger.error('HttpError on %s', response.url)
		elif failure.check(DNSLookupError):
			# this is the original request
			request = failure.request
			self.logger.error('DNSLookupError on %s', request.url)
		elif failure.check(TimeoutError, TCPTimedOutError):
			request = failure.request
			self.logger.error('TimeoutError on %s', request.url)
		

		
