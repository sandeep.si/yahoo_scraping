# -*- coding: utf-8 -*-
import scrapy
import re
import json
import codecs
from scrapy.http import HtmlResponse
from ..items import YahooItem
from scrapy.spidermiddlewares.httperror import HttpError
from twisted.internet.error import DNSLookupError,TimeoutError, TCPTimedOutError
from scraper_api import ScraperAPIClient
from scrapy.exceptions import UsageError
from twisted.python.failure import Failure
from urllib.parse import urlencode
from scrapy.http import HtmlResponse



class ProductsSpider(scrapy.Spider):
	name = 'total'
	allowed_domains = ['loco.yahoo.co.jp','api.scraperapi.com']

	http_error_file = open("http_error_urls.txt", "a")
	product_error_file = open("product_error_urls.txt", "a")
	pagination_error_file = open("pagination_error_urls.txt", "a")
	dns_tcp_error_file = open("dns_tcp_error_urls.txt", "a")
	
	
	main_url = 'https://loco.yahoo.co.jp'
	client = ScraperAPIClient('7cb5caf807e61f97b73f19fc4a0414b1')
	
	def start_requests(self):

		# area_counter={'41':7,'42':14,'44':11,'45':9,'46':24,'47':7}
		area_counter={'11':16,'12':15,'13':38}

		

		# genre_counter = {'01':28,'02':12,'03':8,'04':26,'05':11}
		genre_counter = {'01':28}
		
		for pref in area_counter:
			prefecture_id = pref
			area_count = area_counter[prefecture_id]
			print(prefecture_id)
			print(area_count)
			for i in range(1,area_count+1):
				areacd = str(1) + prefecture_id + '{:02}'.format(i) + '0000'
				for genre in genre_counter:
					genre_id = genre
					genre_count = genre_counter[genre_id]
					print(genre_id)
					print(genre_count)
					for j in range(1,genre_count+1):
						genrecd = genre_id + '{:02}'.format(j)
						url = 'https://loco.yahoo.co.jp/search/?genrecd='+genrecd+'&areacd='+areacd+'&paypay=1'
						yield scrapy.Request(self.client.scrapyGet(url=url), callback=self.parse_initial, method='GET', errback=self.errback_httpbin,meta={'url':url,'function':'start'})



	def parse_initial(self, response):

		print(response.url)

		cards = response.css('div.tracked_mods div.card__item')


		for card in cards:

			card_name = card.css('h3.card__name a::text').extract_first()
			card_access = card.css('div.card__access::text').extract_first()
			card_rating = card.css('span.cardReview__number::text').extract_first()
			card_price_dinner = card.css('div.cardPrice__text--dinner::text').extract_first()
			card_price_lunch = card.css('div.cardPrice__text--lunch::text').extract_first()

			card_dict = dict()
			card_dict['card_name'] = card_name
			card_dict['card_access'] = card_access
			card_dict['card_rating'] = card_rating
			card_dict['card_price_dinner'] = card_price_dinner
			card_dict['card_price_lunch'] = card_price_lunch

			shop_link = card.css('h3.card__name a::attr(href)').extract_first()
			
			card_dict['function'] = 'initial'

			retry = response.meta.get('retry',None)

			if retry is not None:
				card_dict['retry'] = retry + 1
			else:
				card_dict['retry'] = 0



			
			shop_url = ''		
			if shop_link is not None:
				shop_url = self.main_url + shop_link
				card_dict['url'] = shop_url
				yield scrapy.Request(self.client.scrapyGet(url=shop_url), callback=self.parse_shop, method='GET', errback=self.errback_httpbin, meta=card_dict)
				# yield scrapy.Request(url=shop_url, callback=self.parse_shop, method='GET', errback=self.errback_httpbin, meta=card_dict)

		trial = response.meta.get('trial',None)

		if trial is not None:
			trial = trial + 1
		else:
			trial = 0

		

		pagination_items = response.css('ul.pagination li.pagination__item')
		if len(pagination_items) > 0:
			for item in pagination_items:
				next_page_attr = item.css('a::attr(data-ylk)').extract_first()
				if next_page_attr is not None:
					if next_page_attr == 'slk:pn_next':
						pagination = item.css('a::attr(href)').extract_first()
						if pagination is not None:
							url = self.main_url + pagination
							# yield scrapy.Request(url=url, callback=self.parse_initial, method='GET', errback=self.errback_httpbin)
							yield scrapy.Request(self.client.scrapyGet(url=url), callback=self.parse_initial, method='GET', errback=self.errback_httpbin,meta={'url':url,'function':'self_call','trial':trial})

	def parse_shop(self,response):

		print(response.url)

		page_url = response.meta["url"]

		shop_name = response.css('div.placeSummaryName p.placeSummaryName__title::text').extract_first()

		jsondata = response.css('input#tenpojson::attr(value)').extract_first()
		
		data = None
		if jsondata is not None:
			try:
				data = json.loads(jsondata)	
			except json.decoder.JSONDecodeError:
				print('JSONDecodeError occured')
			

		data_list = []

		if data is not None:
			data_list = data.get('Feature',[])

		address = None

		if len(data_list) > 0:
			if data_list[0] is not None:
				property_info = data_list[0].get('Property')
				if property_info is not None:
					address = property_info.get('Address')


		item = YahooItem()
		
		location = address
		contact = ''
		category = ''
		business_hours = ''
		holidays = ''
		paypay_flag = 1

		place_table = response.css('table#placetable tr.placeData__row')
		for row in place_table:
			row_head = row.css('th.placeData__header::text').extract_first()
			if row_head == '電話番号':
				contact = row.css('td.placeData__data span::text').extract_first()
			elif row_head == '営業時間':
				business_hours = row.css('td.placeData__data::text').extract_first()
			elif row_head == 'カテゴリ':
				category = row.css('td.placeData__data::text').extract_first()
			elif row_head == 'ランチ予算':
				lunch = row.css('td.placeData__data::text').extract_first()
			elif row_head == 'ディナー予算':
				dinner = row.css('td.placeData__data::text').extract_first()
			
		external_table = response.css('table#external__table tr.placeData__row')
		for row in external_table:
			row_head = row.css('th.placeData__header::text').extract_first()
			if row_head == '定休日':
				holidays = row.css('td.placeData__data::text').extract_first()



		item['shop_name'] = self.filter_text(response.meta["card_name"])
		item['shop_location'] = self.filter_text(location)
		item['shop_contact'] = self.filter_text(contact)
		item['shop_access'] = self.filter_text(response.meta["card_access"])
		item['shop_category'] = self.filter_text(category)
		item['rating'] = self.filter_text(response.meta["card_rating"])
		item['price_dinner'] = self.filter_text(response.meta["card_price_dinner"])
		item['price_lunch'] = self.filter_text(response.meta["card_price_lunch"])
		item['shop_business_hours'] = self.filter_text(business_hours)
		item['shop_holidays'] = self.filter_text(holidays)
		item['paypay_flag'] = paypay_flag
		item['page_url'] = page_url

		print(item)
		yield item
	   
				

	def filter_text(self, text):
		if text is None:
			text = '-'
		text = text.replace(u'\\u3000', u' ')
		text = text.replace(u'\\xa0', u' ')
		text = text.replace(u'\\n', u' ')
		text = ' '.join(text.split())
		if text == '':
			text = '-'	
		return text

	
	def errback_httpbin(self, failure):
		# log all failures
		self.logger.error(repr(failure))
		# in case you want to do something special for some errors,
		# you may need the failure's type:
		if failure.check(HttpError):
			# these exceptions come from HttpError spider middleware
			# you can get the non-200 response
			response = failure.value.response
			
			url = response.meta.get('url')
			scraper_api_url = response.url
			code = response.status
			function = response.meta.get('function')
			card_dict = response.meta
			if function == 'initial':
				retry = response.meta.get('retry',0)
				if retry < 5:
					retry = retry + 1
					card_dict['retry'] = retry
					yield scrapy.Request(self.client.scrapyGet(url=url), callback=self.parse_shop, method='GET', errback=self.errback_httpbin, meta=card_dict)
				else:
					self.product_error_file = open("product_error_urls.txt", "a")
					self.product_error_file.write(url+','+str(code))
					# self.product_error_file.close()

			elif function == 'self_call':
				trial = response.meta.get('trial',0)
				if trial < 5:
					trial = trial + 1
					yield scrapy.Request(self.client.scrapyGet(url=url), callback=self.parse_initial, method='GET', errback=self.errback_httpbin,meta={'url':url,'function':'self_call','trial':trial})		
				else:
					self.pagination_error_file = open("pagination_error_urls.txt", "a")
					self.pagination_error_file.write(url+','+str(code))
					# self.pagination_error_file.close()		
			else:
				self.http_error_file = open("http_error_urls.txt", "a")
				self.http_error_file.write(url +','+ str(code)+'\n')
				# self.http_error_file.close()
			

			self.logger.error('HttpError on %s', response.url)
		
		elif failure.check(DNSLookupError):
			# this is the original request
			request = failure.request
			scraper_api_url = request.url
			self.dns_tcp_error_file = open("dns_tcp_error_urls.txt", "a")
			self.dns_tcp_error_file.write(scraper_api_url+','+'dns')
			# self.dns_tcp_error_file.close()
			self.logger.error('DNSLookupError on %s', request.url)
		
		elif failure.check(TimeoutError, TCPTimedOutError):
			request = failure.request
			scraper_api_url = request.url
			self.dns_tcp_error_file = open("dns_tcp_error_urls.txt", "a")
			self.dns_tcp_error_file.write(scraper_api_url+','+'tcp')
			# self.dns_tcp_error_file.close()
			self.logger.error('TimeoutError on %s', request.url)

	

		

		
